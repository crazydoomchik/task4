﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using GameLibrary;

[assembly: InternalsVisibleTo("Task4Tests")]
namespace Task4
{
    public class Game
    {
        

        internal int randomNumber;
        private int compareNumber;

        public Game()
        {
            Random rnd = new Random();
            randomNumber = rnd.Next(0, 101);
            compareNumber = -1;
        }
        public Game (int minValue, int maxValue)
        {
            Random rnd = new Random();
            randomNumber = rnd.Next(minValue, maxValue + 1);
            compareNumber = randomNumber + 1;
        }

        public Compare NumberCompare(int number)
        {
            compareNumber = number;
            if (compareNumber < randomNumber) return Compare.LessNumber;
            else if (compareNumber > randomNumber) return Compare.GreatedNumber;

            return Compare.WinGame;
        }

        public bool CheckVictoryConditions()
        {
            return randomNumber == compareNumber;
        }
    }
}
