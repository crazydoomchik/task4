﻿using System;
using System.Configuration;


namespace Task4
{
    public class Config
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }

        public Config()
        {
            MinValue = 0;
            MaxValue = 100;
            GetConfigValues();
        }

        public void GetConfigValues()
        {

            MinValue = ParseConfiValue("MinValue");
            MaxValue = ParseConfiValue("MaxValue");

            if (MinValue > MaxValue || MinValue == MaxValue)
            {
                MinValue = 0;
                MaxValue = 100;
                Console.WriteLine("Wrong values in config file! Used initial values!");
            }

        }

        private int ParseConfiValue(string keyName)
        {
            int value;

            if (!Int32.TryParse(ConfigurationManager.AppSettings.Get(keyName), out value))
            {
                Console.WriteLine($"Wrong {keyName} in config file! Used initial values!");
            }
            return value;
        }
    }
}
