﻿using GameLibrary;
using System;



namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Config config = new Config();
            int minValue = config.MinValue;
            int maxValue = config.MaxValue;

            while (true)
            {
                Game game = new Game(minValue, maxValue);

                Console.WriteLine($"Hello enter your number from {minValue} to {maxValue}. ");

                while (!game.CheckVictoryConditions())
                {
                    int number;

                    if (!Int32.TryParse(Console.ReadLine(), out number) || number < minValue ||number > maxValue)
                    {
                        Console.WriteLine($"Please enter only number from {minValue} to {maxValue}!");
                        continue;
                    }

                    Compare compareResult = game.NumberCompare(number);

                    switch (compareResult)
                    {
                        case Compare.LessNumber:
                            Console.WriteLine("The hidden number is greated");
                            break;
                        case Compare.GreatedNumber:
                            Console.WriteLine("The hidden number is less");
                            break;
                        case Compare.WinGame:
                            Console.WriteLine("You win!");
                            break;
                    }
                }

                Console.WriteLine("Do you want to play more? Press: y; \nOr press any for exid");

                if(Console.ReadLine().ToLower() != "y")
                {
                    return;
                }
            }
        }      
    }
}
