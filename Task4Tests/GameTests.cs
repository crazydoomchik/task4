﻿
using NUnit.Framework;

namespace Task4.Tests
{

    public class GameTests
    {
        [Test()]
        [TestCase(3, -1)]
        [TestCase(55, 1)]
        [TestCase(54, 0)]

        public void NumberCompareTest(int testNmber, int expected)
        {
            //arrange

            //Act
            Game game = new Game();
            game.randomNumber = 54;

            int test = (int)game.NumberCompare(testNmber);

            //Assert
            Assert.AreEqual(expected, test);
        }

        [Test()]
        [TestCase(4, false)]
        [TestCase(20, true)]
        [TestCase(0, false)]
        public void GameMarkerTest(int testNumber, bool expected)
        {
            //Act
            Game game = new Game();

            game.randomNumber = 20;

            game.NumberCompare(testNumber);

            //Assert
            Assert.AreEqual(expected, game.CheckVictoryConditions());
        }
    }
}