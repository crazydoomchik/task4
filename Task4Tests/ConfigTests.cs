﻿using NUnit.Framework;
using System.Configuration;

namespace Task4.Tests
{

    public class ConfigTests
    {
        
       
        [Test()]
        [TestCase(0, 0, 0, 100)]
        [TestCase(5, 1, 0, 100)]
        [TestCase(5000, 0, 0, 100)]
        [TestCase(100, 100, 0, 100)]
        public void GetConfigValuesTest(int test1, int test2, int expected_1, int expected_2)
        {
            Config config = new Config();
            
            config.MinValue = test1;
            config.MaxValue = test2;

            config.GetConfigValues();

            var tupleTest = (config.MinValue, config.MaxValue);
            var tupleExpected = (expected_1, expected_2);

            Assert.AreEqual(tupleTest, tupleExpected);

        }     
    }
}